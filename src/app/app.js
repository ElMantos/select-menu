import React, { PureComponent } from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';

import { SelectMenu, TagInput } from './pages';
import { Navigation } from './components';

import './style.scss';

class App extends PureComponent {
  render() {
    return (
      <div className="bg-gray-300 h-screen">
        <Router>
          <Navigation />
          <Route path="/select" component={SelectMenu} />
          <Route path="/tag" component={TagInput} />
        </Router>
      </div>
    );
  }
}

export default App;
