import React from 'react';
import PropTypes from 'prop-types';

function PersistedItemList({ items, onItemRemove }) {
  return (
    <div className="SelectMenu_persisted_list">
      {items.map((item, index) => (
        <button
          key={`${item}-${index}`}
          type="button"
          onClick={() => onItemRemove(item)}
          className="SelectMenu_persisted_list_item"
        >
          {item}
        </button>
      ))}
    </div>
  );
}

PersistedItemList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  onItemRemove: PropTypes.func.isRequired
};

export default PersistedItemList;
