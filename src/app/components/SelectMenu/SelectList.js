import React from 'react';
import PropTypes from 'prop-types';

function ListItem({ item, onOptionSelect }) {
  return (
    <li className="SelectMenu_list-item">
      <button onClick={() => onOptionSelect(item)} type="button">
        {item}
      </button>
    </li>
  );
}

ListItem.propTypes = {
  item: PropTypes.string.isRequired,
  onOptionSelect: PropTypes.func.isRequired
};

function SelectList({ items, shouldDisplay, filterBy, onOptionSelect }) {
  if (!shouldDisplay) return null;
  const filteredItems = items.filter(item => {
    if (!filterBy) return true;
    return item.toLowerCase().includes(filterBy.toLowerCase());
  });

  return (
    <div className="SelectMenu_list">
      <ul>
        {filteredItems.map((item, index) => {
          return (
            <ListItem
              key={`${item}-${index}`}
              onOptionSelect={onOptionSelect}
              item={item}
            />
          );
        })}
      </ul>
    </div>
  );
}

SelectList.propTypes = {
  filterBy: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  shouldDisplay: PropTypes.bool.isRequired,
  onOptionSelect: PropTypes.func.isRequired
};

export default SelectList;
