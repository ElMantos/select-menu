import React, { useState } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import onClickOutside from 'react-onclickoutside';

import SelectList from './SelectList';
import PersistedItemList from './PersistedItemList';
import { useFocus } from '../../hooks';

import './selectmenu.scss';

const handleTextChange = (event, changeValue) => {
  changeValue(event.target.value);
};

const handleNonPersistChange = (text, changeValue, setFocused) => {
  changeValue(text);
  setFocused(false);
};

const getInputWidth = text => {
  if (!text) return '10px';

  return `${text.length * 10}px`;
};

const handlePersistChange = (
  text,
  persistedItems,
  setPersistedItems,
  onSelect
) => {
  const newItems = [...persistedItems, text];
  setPersistedItems(newItems);
  onSelect(newItems);
};

const removeSelectedFromItems = (items, persistedItems) => {
  const newItems = items.filter(item => !persistedItems.includes(item));
  return newItems;
};

const removeItemFromPersisted = (
  item,
  persistedItems,
  setPersistedItems,
  onSelect
) => {
  const newItems = persistedItems.filter(
    persistedItem => item !== persistedItem
  );
  setPersistedItems(newItems);
  onSelect(newItems);
};

function SelectMenu({ defaultValue, items, shouldPersistValues, onSelect }) {
  const [textValue, setTextValue] = useState(defaultValue);
  const [persistedItems, setPersistedItems] = useState([]);
  const [isFocused, setFocused] = useState(false);
  const [inputRef, setInputFocus] = useFocus();

  const handleOptionSelect = text => {
    if (shouldPersistValues) {
      handlePersistChange(text, persistedItems, setPersistedItems, onSelect);
      setTextValue('');
      setInputFocus(true);
    } else {
      handleNonPersistChange(text, setTextValue, setFocused);
      onSelect(text);
    }
  };

  SelectMenu.handleClickOutside = () => setFocused(false);
  return (
    <div className="SelectMenu">
      <div
        onClick={() => setInputFocus(true)}
        tabIndex={0}
        role="textbox"
        onKeyPress={() => setInputFocus(true)}
        className={cx(
          'SelectMenu_container',
          isFocused && 'SelectMenu_container-focused'
        )}
      >
        {shouldPersistValues && (
          <PersistedItemList
            onItemRemove={item =>
              removeItemFromPersisted(
                item,
                persistedItems,
                setPersistedItems,
                onSelect
              )
            }
            items={persistedItems}
          />
        )}
        <input
          ref={inputRef}
          type="text"
          style={{
            width: getInputWidth(textValue)
          }}
          className="SelectMenu_input"
          onFocus={() => setFocused(true)}
          value={textValue}
          onChange={event => handleTextChange(event, setTextValue)}
        />
      </div>
      <SelectList
        items={removeSelectedFromItems(items, persistedItems)}
        onOptionSelect={text => handleOptionSelect(text)}
        filterBy={textValue}
        shouldDisplay={isFocused}
      />
    </div>
  );
}

SelectMenu.propTypes = {
  defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  shouldPersistValues: PropTypes.bool.isRequired,
  onSelect: PropTypes.func.isRequired
};

const clickOutsideConfig = {
  handleClickOutside: () => SelectMenu.handleClickOutside
};

export default onClickOutside(SelectMenu, clickOutsideConfig);
