import React from 'react';

import NavItem from './NavItem';

import './navigation.scss';

function Navigation() {
  return (
    <nav>
      <ul className="flex w-screen h-12 items-center justify-center">
        <NavItem text="Tag Input" link="/tag" />
        <NavItem text="Select Menu" link="/select" />
      </ul>
    </nav>
  );
}

export default Navigation;
