import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

function NavItem({ text, link }) {
  return (
    <li className="mr-2">
      <Link to={link}>
        <span className="NAVIGATION_navitem hover:bg-gray-700 hover:text-white">
          {text}
        </span>
      </Link>
    </li>
  );
}

NavItem.propTypes = {
  text: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired
};

export default NavItem;
