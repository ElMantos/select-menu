import React, { useState } from 'react';

import { SelectMenu } from '../components';

function TagInput() {
  const items = [
    'Cafe',
    "Mayor's Office",
    'Train Station',
    'Alley',
    'Ballroom',
    'Clothing Store',
    'Marketplace',
    '	Garden',
    'Docks',
    'Dressing Room'
  ];

  const [selectedTags, setSelectedTags] = useState([]);
  return (
    <div className="flex flex-col items-center justify-center mt-24">
      <div className="mb-6 h-2">
        {selectedTags.length > 0 && 'You can remove a tag by clicking on it!'}
      </div>
      <SelectMenu
        shouldPersistValues
        onSelect={setSelectedTags}
        defaultValue=""
        items={items}
      />
      <div className="mt-6">
        {selectedTags.length ? (
          <ul>
            {selectedTags.map((tag, index) => (
              <li key={`${tag}-${index}`}>{tag}</li>
            ))}
          </ul>
        ) : (
          'Select at least one tag'
        )}
      </div>
    </div>
  );
}

export default TagInput;
