import React, { useState } from 'react';

import { SelectMenu as SelectMenuInput } from '../components';

function SelectMenu() {
  const items = [
    'Cafe',
    "Mayor's Office",
    'Train Station',
    'Alley',
    'Ballroom',
    'Clothing Store',
    'Marketplace',
    '	Garden',
    'Docks',
    'Dressing Room'
  ];

  const [selectedValue, setSelectedValue] = useState(null);

  return (
    <div className="flex flex-col items-center justify-center mt-24">
      <SelectMenuInput
        onSelect={setSelectedValue}
        shouldPersistValues={false}
        defaultValue=""
        items={items}
      />
      <div className="mt-6">
        <span>
          {selectedValue ? `Selected Value:${selectedValue}` : 'Select a value'}
        </span>
      </div>
    </div>
  );
}

export default SelectMenu;
