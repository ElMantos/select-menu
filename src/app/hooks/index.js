import { useRef } from 'react';

export const useFocus = () => {
  const htmlElRef = useRef(null);
  const setFocus = shouldFocus =>
    shouldFocus ? htmlElRef.current.focus() : htmlElRef.current.blur();

  return [htmlElRef, setFocus];
};
